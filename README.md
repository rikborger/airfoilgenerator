# README #
This is the readme for a simple NACA airfoil geometry data creation tool. The created geometry output can be used to generate airfoil shapes in, for example, Ansys ICEM CFD. 

## Possible improvements ##
 * Add support for 5/6 digit NACA series
 * Improve GUI

## Contact ##
If you want to contribute, feel free to contact me.
//Isolocis